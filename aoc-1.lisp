(defun aoc-1-1 ()
  (with-open-file (input "input-1.txt")
    (when input
      (loop for l = (read-line input nil)
            while l
            summing (parse-integer l)))))

(defun aoc-1-2 ()
  (let ((il (with-open-file (input "input-1.txt")
              (loop for l = (read-line input nil)
                    while l
                    collecting (parse-integer l)))))
    (setf (cdr (last il)) il)
    (loop for f in il
          with seen = (make-hash-table)
          summing f into partial-sum
          until (gethash partial-sum seen)
          do (setf (gethash partial-sum seen) t)
          finally (return partial-sum))))
