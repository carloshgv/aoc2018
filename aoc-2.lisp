(defvar *input* (uiop:read-file-lines "input-2.txt"))

(defun partial (fn &rest args)
  (lambda (&rest more)
    (apply fn (append args more))))

(defun sort-string (s)
  (sort (copy-seq s) #'char<))

(defun count-repeats (s)
  (loop for c across s
        with count = 0 and last = (char s 0)
        when (char= last c) do (incf count)
        else collect count into counts and do (progn (setf count 1) (setf last c))
        finally (return (push count counts))))

(defun aoc-2-1 ()
  (let* ((sorted (mapcar #'sort-string *input*))
         (counted (mapcar #'count-repeats sorted))
         (twos (count-if (partial #'find 2) counted))
         (threes (count-if (partial #'find 3) counted)))
    (* twos threes)))

(defun compare-letters (s1 s2)
  (let ((diff (map 'vector #'char= s1 s2)))
    (values diff (count nil diff))))

(defun remove-flagged (s flags)
  (loop for c across s
        for f across flags
        when f collect c into list-of-chars
        finally (return (format nil "~{~A~}" list-of-chars))))

(defun find-match/2 (code list)
  (when list
    (multiple-value-bind (matches count) (compare-letters code (car list))
      (if (= count 1)
          (values (car list) matches)
          (find-match/2 code (cdr list))))))

(defun find-match/1 (list)
  (when list
    (multiple-value-bind (match positions) (find-match/2 (car list) (cdr list))
      (if match
          (values (cons (car list) match) positions)
          (find-match/1 (cdr list))))))

(defun aoc-2-2 ()
  (multiple-value-bind (pair flags) (find-match/1 *input*)
    (remove-flagged (car pair) flags)))
