(defpackage :aoc-3
  (:use :cl :cl-ppcre))

(in-package :aoc-3)

(defvar *input* (uiop:read-file-lines "input-3.txt"))
(defvar *parsed* (mapcar #'parse-coords *input*))

(defun parse-coords (str)
  (register-groups-bind (id x y w h)
      ("#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)" str)
    (mapcar #'parse-integer (list id x y w h))))

(defparameter *matrix* (make-array '(1000 1000) :initial-element '()))

(defun cover (id x0 y0 w h)
  (loop for x from x0 below (+ x0 w)
     do (loop for y from y0 below (+ y0 h)
           do (push id (aref *matrix* x y)))))

(defun cover-all ()
  (dolist (rect *parsed*)
    (cover (first rect) (second rect) (third rect) (fourth rect) (fifth rect))))

(defun partial (fn &rest args)
  (lambda (&rest more) (apply fn (append args more))))

(defun more-than-1 (lst)
  (< 1 (length lst)))

(defun just-1 (lst)
  (= 1 (length lst)))

(cover-all)

(defun aoc-3-1 ()
  (let ((linear (make-array (* 1000 1000) :displaced-to *matrix*)))
    (values (count-if #'more-than-1 linear) (find-if #'just-1 linear))))

(defun aoc-3-2 ()
  (let ((ids (mapcar #'first *parsed*))
        (linear (make-array (* 1000 1000) :displaced-to *matrix*)))
    (loop for x across linear
       when (more-than-1 x) do (dolist (id x) (setf ids (delete id ids)))
       finally (return ids))))
