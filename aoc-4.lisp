(defpackage :aoc-4
  (:use :cl))

(in-package :aoc-4)

(defvar *input* (sort (uiop:read-file-lines "input-4.txt") #'string<))
